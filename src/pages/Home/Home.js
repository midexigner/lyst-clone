import React from "react";
import ArticleCard from "../../components/Article/ArticleCard";
import Banner from "../../components/Banner/Banner";
import ClientCard from "../../components/ClientCard/ClientCard";
import NewsCard from "../../components/NewsCard/NewsCard";
import Trending from "../../components/Trending/Trending";
import "./Home.css";

function Home() {
  return (
    <div className="home__page">
      <Banner />
      {/* banner */}
      {/* Trending now */}
      <Trending />
      {/* Logos */}
      <div className="clients">
            <div className="client__contain">
            { [1, 2, 3,4,5,6,7,8,9,10].map((item,index)=>(
            <ClientCard 
            key={Math.random()}
            image="https://cdna.lystit.com/cms/acne_studios_ef9a8e3aa2.png" 
            title="Acne Studios"/>
            ))}
            </div>
        </div>
      {/* news */}
      <div className="news">
       <div className="news__conatin">
      { [1, 2, 3].map((item,index)=>(<
        NewsCard
        key={index}
        img="https://cdna.lystit.com/cms/NEC_Paid_Hypebeast_4_2_1c0e7b3e1d.jpg"
        url="/abc"
        title="Introducing RIMOWA Essential Desert Rose"
        excerpt="Luggage expertly color matched for a look that is truly unique"
        btnText="More Info"
        />))} 
        </div> 
      </div>
      <div className="article">
        <div className="article__conatin">
          <ArticleCard 
          img="https://cdna.lystit.com/cms/Lyst_Index_Assets_Launch_Site_Launch_Module_V2_2f6f5d06fd.jpg" 
          title="The Lyst Index" 
          description="The definitive ranking of fashion’s hottest brands and products, based on data insights from over 100 million real shoppers." 
          url="/clothing" 
          btnText="Discover the latest lists"
          /> 
          </div> 
          <div className="article__conatin">
          <ArticleCard 
          isRight
          img="https://cdna.lystit.com/cms/business_value_app_650201085b90_f61d1f47ad.jpeg" 
          title="The Lyst Index" 
          description="The definitive ranking of fashion’s hottest brands and products, based on data insights from over 100 million real shoppers." 
          url="/ios-download" 
          btnText="Download the app"
          />
          </div>
      </div>
      {/* single article 1 */}
      {/* single article 2 */}
      {/* Lyst Loves */}
      {/* Popular Lyst searches */}
      {/* categories */}
    </div>
  );
}

export default Home;
