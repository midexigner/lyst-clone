import React from 'react'
import BannerCard from '../BannerCard/BannerCard'
import './Banner.css'

const Banner = () => {
    return (
        <div className="banner">
            <div className="banner__container">
                <h1 className="banner__container__title">
                Your fashion search starts here.
                </h1>
                <div className="banner__cards">
                    <BannerCard image="https://static.lystit.com/static/n/img/header-backgrounds/women-shop-now.3392a0fc87480144dd0bbc983df2b2a4.jpg" title="Women" ButtonUrl="/women"/>
                    <BannerCard image="https://static.lystit.com/static/n/img/header-backgrounds/men-shop-now.6b6ab4750857166f8bc25d5f1096cacf.jpg" title="Men" ButtonUrl="/men"/>
                </div>
            </div>
        </div>
    )
}

export default Banner
