import React from 'react'
import './BannerCard.css'
import Button from '@material-ui/core/Button';

const  BannerCard = ({title,image,ButtonUrl})=> {
    return (
 <div className="banner__card" style={{backgroundImage:"url("+`${image}`+")"}}>
            <div className="banner__cardContain">
            <h2>{title}</h2>
          <Button variant="contained" onClick={()=> {window.location.href=`${ButtonUrl}`;}}>Shop Now</Button>
          </div>
        </div>
    )
}

export default BannerCard
