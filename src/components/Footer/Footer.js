import React from "react";
import { Link } from "react-router-dom";
import "./Footer.css";

const Footer = () => {
  return (
    <footer>
      <div className="contain">
        <div className="col-1">
          <Link to="/">
            <img
              className="ios__appIcon"
              src="https://static.lystit.com/static/n/img/ui/app-store-white.0f3514a45d51f95167e5fe8b6a03bb60.svg"
              alt=""
            />
          </Link>
          <p>Learn about the new Lyst app for iPhone and iPad.</p>
          <p>&copy; {new Date().getFullYear()} Lyst</p>
        </div>
        <div className="col-2">
          <h4>Women</h4>
          <ul className="list__inline">
            <li>
              <Link to="/">Ankle boots</Link>
            </li>
            <li>
            <Link to="/">Cocktail dresses</Link>
            </li>
            <li>
            <Link to="/">Gowns</Link>
            </li>
            <li>
            <Link to="/">Leather jackets</Link>
            </li>
            <li>
            <Link to="/">Maxi and long dresses</Link>
            </li>
            <li>
            <Link to="/"> Skinny jeans</Link>
            </li>
            <li>
            <Link to="/"> Sweaters and pullovers</Link>
            </li>
          </ul>
          <Link to="/" className="view__allLink">View all </Link>
        </div>
        <div className="col-2">
        <h4>Men</h4>
          <ul className="list__inline">
            <li>
              <Link to="/">Leather jackets</Link>
            </li>
            <li>
            <Link to="/">Oxfords</Link>
            </li>
            <li>
            <Link to="/">Polo shirts</Link>
            </li>
            <li>
            <Link to="/">Raincoats and trench coats</Link>
            </li>
            <li>
            <Link to="/">Loafers</Link>
            </li>
            <li>
            <Link to="/">High-top sneakers</Link>
            </li>
            <li>
            <Link to="/">Hoodies</Link>
            </li>
          </ul>
          <Link to="/" className="view__allLink">View all </Link>
        </div>
        <div className="col-2">
        <h4>Help and info</h4>
          <ul className="list__inline">
            <li>
              <Link to="/">Help center</Link>
            </li>
            <li>
            <Link to="/">About us</Link>
            </li>
            <li>
            <Link to="/">Shipping policy</Link>
            </li>
            <li>
            <Link to="/">Returns policy</Link>
            </li>
            <li>
            <Link to="/">Developers</Link>
            </li>
            <li>
            <Link to="/">Refund policy</Link>
            </li>
            <li>
            <Link to="/">Careers</Link>
            </li>
            <li>
            <Link to="/">Contact</Link>
            </li>
            <li>
            <Link to="/">Terms & conditions</Link>
            </li>
            <li>
            <Link to="/">Privacy & cookie policy</Link>
            </li>
            <li>
            <Link to="/">Intellectual property</Link>
            </li>
            <li>
            <Link to="/">Become a partner</Link>
            </li>
            <li>
            <Link to="/">Lyst News</Link>
            </li>
            <li>
            <Link to="/">Categories</Link>
            </li>
          </ul>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
