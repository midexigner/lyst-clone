import React from "react";
import { Link } from "react-router-dom";
import "./NewsCard.css"
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
function NewsCard({
    img,
    title,
    excerpt,
    url,
    btnText
}) {
  return (
    <div className="newsCard">
      <img src={img} alt={title} />
      <div className="newsCard__caption">
        <h2>{title}</h2>
        <p>{excerpt}</p>
        <Link to={url}>{btnText} <ChevronRightIcon/></Link>
      </div>
    </div>
  );
}

export default NewsCard;
