import React from 'react'
import { Link } from 'react-router-dom'
import './Header.css'
import SearchIcon from '@material-ui/icons/Search';
import ShoppingBasketIcon from '@material-ui/icons/ShoppingBasket';
const Header = () =>{
    return (
        <div className="header__main">
         <div className="header__top">
             <div className="header_topLeft">
             <p>Search thousands of fashion stores in one place</p>
             </div>
             <div className="header_topRight">
            <div className="header_link">
            <Link to="signin">Join</Link>
            </div>
            <div className="header_link">
            <Link to="signin">Sign In</Link>
            </div>
            <div className="header_link horizontalLineNone">
            <Link to="signin">Help</Link>
            </div>
            <div className="header_link horizontalLineNone">
            <Link to="signin">PK - US$</Link>
            </div>
             </div>
         </div>
         <header>
             <img 
             className="header__logo"
             src="https://static.lystit.com/static/n/static-img/branding/lyst-logo-2020-white.b06dbc9b827e.svg" 
             alt="logo"
             />
             <div className="header__menu">
             <div className="header_link horizontalLineNone">
            <Link to="women">Women</Link>
            </div>
             <div className="header_link horizontalLineNone">
            <Link to="men">Men</Link>
            </div>
             </div>
             <form className="header__search">
                <div className="header__search_Input">
                <input
                type="text"
                placeholder='Search (e.g. "Acne jeans")'
                />
                <SearchIcon/>
                </div>
             </form>
             <div className="header__cart">
        <Link to="/cart">
        <ShoppingBasketIcon/>
        <span>basket</span>
        </Link>
             </div>
         </header>
        </div>
    )
}

export default Header
