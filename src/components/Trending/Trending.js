import React from 'react'
import ProductCard from '../ProductCard/ProductCard'
import "./Trending.css"
import 'swiper/swiper-bundle.min.css';
import SwiperCore, { Navigation} from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';

const Trending = () => {
// install Swiper components
SwiperCore.use([Navigation]);
    
      
    return (
        <div className="trending__now">
            <div className="trending__now_title">
            <h2>Trending right now</h2>
            </div>
        <div className="product__list">
            <Swiper spaceBetween={50}
        slidesPerView={7}
        navigation
      
      >  { [1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15].map((item,index)=>(
            <SwiperSlide  key={Math.random()}> 
            <ProductCard 
            id="101"
            wishlist
            title="The North Face" 
            desc="1996 Nuptse Down Jacket - Black" 
            currencySymbol="$"
            price="258" 
            discount="322" 
            percent="20"  
            imgUrl="https://cdna.lystit.com/200/250/tr/photos/sevenstore/5858d876/the-north-face-Black-1996-Retro-Nuptse-Jacket.jpeg"
            url="/clothing/the-north-face-1996-retro-nupstse-jacket/"
            /></SwiperSlide>
            ))}
            </Swiper>
        </div>
        </div>
    )
}

export default Trending
