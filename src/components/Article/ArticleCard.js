import React from "react";
import { Link } from "react-router-dom";
import "./ArticleCard.css";
const ArticleCard = ({ img, title, description, url, btnText,isRight }) => {
  return (
    <div className={`articleCard ${isRight && "articleCardRight"}`}>
      <img src={img ? img : null} alt={title} />
      <div className="articleCard__desc">
        <h3>{title}</h3>
        <p>{description}</p>
        <Link to={url}>{btnText}</Link>
      </div>
    </div>
  );
};

export default ArticleCard;
