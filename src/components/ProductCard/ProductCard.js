import React from 'react'
import { Link } from 'react-router-dom'
import './ProductCard.css'
import Button from '@material-ui/core/Button';
import FavoriteBorderIcon from '@material-ui/icons/FavoriteBorder';

const  ProductCard = (
    {
        title,
        price,
        discount,
        imgUrl,
        desc,
        wishlist,
        percent,
        url,
        currencySymbol
    }) => {
    return (
        <div className="productCard">
            <img 
            className="productCard__image"
            src={imgUrl}
            alt={title}
            />
            <div className="productCard__details">
            <div className="productCard__wishlist">
            {wishlist ? 
            <Button> 
            <FavoriteBorderIcon/>
            </Button>: null}
            </div>
            <Link to={url} className="product-card__short-description">
<div className="productCard__title">{title}</div>
<div className="productCard__shortDescName">{desc}</div>
</Link>
            <div className="productCard__price_sec">
            <svg className="w-2 h-2" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M7 7h.01M7 3h5c.512 0 1.024.195 1.414.586l7 7a2 2 0 010 2.828l-7 7a2 2 0 01-2.828 0l-7-7A1.994 1.994 0 013 12V7a4 4 0 014-4z" /></svg>
            <del className="productCard__price">{currencySymbol} {discount}</del>
<span className="productCard__price productCard__price__sale">{price}</span>
<span className="productCard__price__discountInfo">({percent}% off)</span>
</div>
</div>
        </div>
    )
}

export default ProductCard
