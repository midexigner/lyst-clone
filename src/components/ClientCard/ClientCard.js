import React from 'react'
import { Link } from 'react-router-dom'
import './ClientCard.css'

const ClientCard =({image,title})=> {
    return (
        <div 
        className="clientCard">
           <img 
           src={image} 
           alt="" 
           />
           <Link to="">{title}</Link>
        </div>
    )
}

export default ClientCard
