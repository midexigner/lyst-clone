import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";

import './App.css';
import Footer from './components/Footer/Footer';
import Header from './components/Header/Header';
import Home from './pages/Home/Home';

function App() {
  return (
    <div className="app">
      <Router>
      <Switch>
      <Route path="/about"><h2>ABout</h2></Route>
      <Route path="/users"><h2>User</h2></Route>
      <Route path="/">
      {/* Header */}
      <Header/>
      <Home/>
      {/* Footer */}
      <Footer/>
      </Route>
      </Switch>
    </Router>
      
    </div>
  );
}

export default App;
